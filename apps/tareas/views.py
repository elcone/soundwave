from django.views.generic import TemplateView
from django.shortcuts import render
from rest_framework import viewsets
from .serializers import *
from .models import *

class TareaViewSet(viewsets.ModelViewSet):
    serializer_class = TareaSerializer
    queryset = Tarea.objects.all()


class Inicio(TemplateView):
    template_name = "inicio.html"
