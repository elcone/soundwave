from django.conf.urls import url, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'tareas', TareaViewSet)

urlpatterns = [
    url(r'api/', include(router.urls)),
    url(r'^$', Inicio.as_view(), name='inicio')
]
