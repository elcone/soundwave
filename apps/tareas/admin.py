from django.contrib import admin
from .models import *

@admin.register(Tarea)
class TareaAdmin(admin.ModelAdmin):
    list_display = ['descripcion', 'terminada']
